﻿using System;
using System.Collections.Generic;
using System.IO;
using ErmProcessor.Classifiers;
using ErmProcessor.Data;

namespace ErmProcessor.ConsoleInterface.Processors {
    public class OutlierProcessor : IProcessor {
        public void Process(string directory) {
            if (directory == null)
                throw new ArgumentNullException(nameof(directory));

            if (!Directory.Exists(directory)) {
                throw new ArgumentException($"{directory} is not a " +
                                            "valid directory path.");
            }

            var files = new List<string>(
                Directory.EnumerateFiles(directory)
            );

            foreach (string filePath in files) {
                using (var fileStream = File.OpenRead(filePath)) {
                    var inputReader = new CsvReader(fileStream);
                    string fileName = Path.GetFileName(filePath);

                    var type = this.GetType(fileName);
                    switch (type) {
                        case Type.Lp:
                            var lpRecords = new LpRecordList(inputReader);
                            this.ProcessRecords(
                                lpRecords,
                                r => r.DataValue.Value,
                                fileName
                            );
                            break;
                        case Type.Tou:
                            var touRecords = new TouRecordList(inputReader);
                            this.ProcessRecords(
                                touRecords,
                                r => r.Energy.Value,
                                fileName
                            );
                            break;
                        case Type.Other:
                            break; // skip this file
                        default:
                            // won't happen
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
        }

        protected void ProcessRecords<T>(RecordList<T> records,
            Func<T, double> valueFunc, string fileName) where T : Record {
            // Create classifier
            var classifier = new SimpleOutlierClassifier<T>(records, valueFunc);
            classifier.SetUp();
            foreach (var record in records) {
                if (classifier.IsOutlier(record)) {
                    Console.WriteLine(
                        $"{fileName} {record.DateTime} {valueFunc(record)} " +
                        $"{classifier.Median}" +
                        $"");
                }
            }
        }

        protected Type GetType(string fileName) {
            if (fileName.Substring(0, 3) == "LP_") return Type.Lp;
            if (fileName.Substring(0, 4) == "TOU_") return Type.Tou;
            return Type.Other;
        }

        protected enum Type {
            Lp,
            Tou,
            Other
        }
    }
}
