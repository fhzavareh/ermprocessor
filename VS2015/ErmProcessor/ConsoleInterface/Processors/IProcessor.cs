﻿namespace ErmProcessor.ConsoleInterface.Processors {
    public interface IProcessor {
        void Process(string directory);
    }
}
