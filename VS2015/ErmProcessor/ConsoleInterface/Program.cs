﻿using System;
using System.Collections.Generic;
using ErmProcessor.ConsoleInterface.Processors;

namespace ErmProcessor.ConsoleInterface {
    public class Program {
        // Plug in new processors here
        private static readonly ICollection<IProcessor> Processors =
            new List<IProcessor> {
                new OutlierProcessor()
            };

        public static void Main(string[] args) {
            if (args == null || args.Length == 0) {
                PrintUsage();
                Console.Write("Input arguments: ");
                args = Console.ReadLine()?.Split(' ');
            }

            if (args == null || args.Length != 1) {
                PrintUsage();
                Console.ReadLine();
                return;
            }

            try {
                string directory = args[0];

                foreach (var processor in Processors) {
                    processor.Process(directory);
                }

                Console.ReadLine();
            } catch (Exception ex) {
                Console.WriteLine($"An error occurred: {ex.Message}");
                Console.ReadLine();
            }
        }

        private static void PrintUsage() {
            Console.WriteLine("Usage: ConsoleInterface in_dir_path");
        }
    }
}
