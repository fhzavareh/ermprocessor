﻿using System;

namespace ErmProcessor.Data {
    public class TouRecord : Record {
        public double? Energy { get; set; }
        public double? MaximumDemand { get; set; }
        public DateTime? MaxDemandDateTime { get; set; }
        public string Period { get; set; }
        public bool? DlsActive { get; set; }
        public int? BillingResetCount { get; set; }
        public DateTime? BillingResetDateTime { get; set; }
        public string Rate { get; set; }
    }
}
