﻿using System;

namespace ErmProcessor.Data {
    public class ParsingException : Exception {
        public ParsingException() { }

        public ParsingException(string message) : base(message) {
        }
    }
}
