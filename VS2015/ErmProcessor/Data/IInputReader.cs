﻿using System.Collections.Generic;

namespace ErmProcessor.Data {
    public interface IInputReader<TKey, TValue> {

        /// <summary>
        /// Reads the next record.
        /// </summary>
        /// <returns>
        /// A record as an <code>IDictionary</code> mapping keys to values.
        /// Returns <code>null</code> if all records have already been read.
        /// </returns>
        IDictionary<TKey, TValue> ReadNext();

        bool HasNext();

        /// <summary>
        /// Resets the <code>IInputReader</code> to the beginning.
        /// </summary>
        void Reset();
    }
}
