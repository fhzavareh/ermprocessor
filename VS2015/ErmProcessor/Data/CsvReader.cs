﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ErmProcessor.Data {
    public class CsvReader : IInputReader<string, string> {
        private readonly Stream inputStream;
        private readonly StreamReader inputStreamReader;
        private readonly ICollection<string> keys;
        private int lineNumber; // last line read

        public CsvReader(Stream inputStream) {
            if (inputStream == null)
                throw new ArgumentNullException(nameof(inputStream));

            this.inputStream = inputStream;
            this.inputStreamReader = new StreamReader(this.inputStream);
            this.keys = new LinkedList<string>();
        }

        public IDictionary<string, string> ReadNext() {
            // If first time, process definition first
            if (this.lineNumber == 0) {
                this.ReadDefinitionLine();
            }

            var vals = this.ReadLine();

            if (vals.Length != this.keys.Count) {
                throw new Exception("Invalid number of values at line " +
                                    $"{this.lineNumber}.");
            }

            var dict = new Dictionary<string, string>();

            int index = 0;
            foreach (string key in this.keys) {
                dict[key] = vals[index++];
            }

            return dict;
        }

        public bool HasNext() {
            return !this.inputStreamReader.EndOfStream;
        }

        public void Reset() {
            this.inputStream.Position = 0;
            this.inputStreamReader.DiscardBufferedData();
            this.lineNumber = 0;
        }

        private void ReadDefinitionLine() {
            if (this.lineNumber != 0) {
                throw new InvalidOperationException(
                    "Stream not at first line."
                );
            }

            var keyList = this.ReadLine();
            if (keyList == null) {
                throw new Exception("Input inputStream has no definition line.");
            }

            this.keys.Clear(); // just to be sure
            foreach (string s in keyList) {
                this.keys.Add(s);
            }
        }

        private string[] ReadLine() {
            this.lineNumber++;
            string line = this.inputStreamReader.ReadLine();
            return line?.Split(',');
        }
    }
}
