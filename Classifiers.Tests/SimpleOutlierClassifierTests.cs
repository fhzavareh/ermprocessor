﻿using System.Collections.Generic;
using System.Linq;
using ErmProcessor.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ErmProcessor.Classifiers.Tests {
    [TestClass]
    public class SimpleOutlierClassifierTests {
        [TestMethod]
        public void Median_ReturnsCorrectValue() {
            // Arrange
            this.GetTestRecords(out var recordList, out double median);
            var classifier = new SimpleOutlierClassifier<LpRecord>(recordList,
                r => r.DataValue.Value);
            classifier.SetUp();

            // Act
            double actualMedian = classifier.Median;

            // Assert
            Assert.AreEqual(median, actualMedian);
        }

        [TestMethod]
        public void IsOutlier_ReturnsTrue_WhenOutlier() {
            this.GetTestRecords(out var recordList, out double median);
            var classifier = new SimpleOutlierClassifier<LpRecord>(recordList,
                r => r.DataValue.Value);
            classifier.SetUp();

            var testRecord = new LpRecord() {
                DataValue = median * 1.25
            };

            bool outlier = classifier.IsOutlier(testRecord);

            Assert.IsTrue(outlier);
        }

        [TestMethod]
        public void IsOutlier_ReturnsTrue_WhenBorderHigh() {
            this.GetTestRecords(out var recordList, out double median);
            var classifier = new SimpleOutlierClassifier<LpRecord>(recordList,
                r => r.DataValue.Value);
            classifier.SetUp();

            var testRecord = new LpRecord() {
                DataValue = median * 1.2
            };

            bool outlier = classifier.IsOutlier(testRecord);

            Assert.IsTrue(outlier);
        }

        [TestMethod]
        public void IsOutlier_ReturnsTrue_WhenBorderLow() {
            this.GetTestRecords(out var recordList, out double median);
            var classifier = new SimpleOutlierClassifier<LpRecord>(recordList,
                r => r.DataValue.Value);
            classifier.SetUp();

            var testRecord = new LpRecord() {
                DataValue = median * .8
            };

            bool outlier = classifier.IsOutlier(testRecord);

            Assert.IsTrue(outlier);
        }

        [TestMethod]
        public void IsOutlier_ReturnsFalse_WhenBorderHigh() {
            this.GetTestRecords(out var recordList, out double median);
            var classifier = new SimpleOutlierClassifier<LpRecord>(recordList,
                r => r.DataValue.Value);
            classifier.SetUp();

            var testRecord = new LpRecord() {
                DataValue = (median * 1.2) - .0001
            };

            bool outlier = classifier.IsOutlier(testRecord);

            Assert.IsFalse(outlier);
        }

        [TestMethod]
        public void IsOutlier_ReturnsFalse_WhenBorderLow() {
            this.GetTestRecords(out var recordList, out double median);
            var classifier = new SimpleOutlierClassifier<LpRecord>(recordList,
                r => r.DataValue.Value);
            classifier.SetUp();

            var testRecord = new LpRecord() {
                DataValue = (median * .8) + .0001
            };

            bool outlier = classifier.IsOutlier(testRecord);

            Assert.IsFalse(outlier);
        }

        private void GetTestRecords(out IEnumerable<LpRecord> records,
            out double median) {
            records = new List<LpRecord>
                {
                    new LpRecord() {DataValue = 2},
                    new LpRecord() {DataValue = 3},
                    new LpRecord() {DataValue = 4},
                    new LpRecord() {DataValue = 1},
                    new LpRecord() {DataValue = 5},
                    new LpRecord() {DataValue = 2}
                };
            double sum = records.Sum(
                   lpRecord => lpRecord.DataValue.Value
               );
            median = sum / records.Count();
        }
    }
}
