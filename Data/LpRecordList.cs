﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ErmProcessor.Data {
    public class LpRecordList : RecordList<LpRecord> {
        private const string DATA_VALUE = "Data Value";

        private readonly IInputReader<string, string> inputReader;

        public LpRecordList(IInputReader<string, string> inputReader) {
            if (inputReader == null)
                throw new ArgumentNullException(nameof(inputReader));

            this.inputReader = inputReader;
        }

        public override IEnumerator<LpRecord> GetEnumerator() {
            return new Enumerator(this);
        }

        private class Enumerator : IEnumerator<LpRecord> {
            private readonly LpRecordList parent;
            private readonly IInputReader<string, string> inputReader;

            private LpRecord current;

            public Enumerator(LpRecordList parent) {
                this.parent = parent;
                this.inputReader = this.parent.inputReader;
                this.inputReader.Reset();
            }

            public bool MoveNext() {
                if (!this.inputReader.HasNext()) return false;

                var properties = this.inputReader.ReadNext();

                var record = new LpRecord();
                this.parent.SetBasicProperties(record, properties);
                record.DataValue = this.parent.ReadDoubleValue(DATA_VALUE, properties);

                this.current = record;

                return true;
            }

            public void Reset() {
                this.inputReader.Reset();
                this.current = null;
            }

            public LpRecord Current {
                get {
                    if (this.current == null) {
                        throw new InvalidOperationException();
                    }
                    return this.current;
                }
            }

            object IEnumerator.Current => Current;

            public void Dispose() { }
        }
    }
}
