﻿using System;

namespace ErmProcessor.Data {
    public class Record {
        // Allow null for all values to provide flexibility
        // if loading partial data is needed
        public int? MeterPointCode { get; set; }
        public int? SerialNumber { get; set; }
        public string PlantCode { get; set; }
        public DateTime? DateTime { get; set; }
        public string DataType { get; set; }
        public string Units { get; set; }
        public string Status { get; set; }
    }
}
