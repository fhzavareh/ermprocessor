﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ErmProcessor.Data {
    public class TouRecordList : RecordList<TouRecord> {
        #region Constant Property Keys
        private const string ENERGY = "Energy";
        private const string MAXIMUM_DEMAND = "Maximum Demand";
        private const string MAX_DEMAND_DATETIME = "Time opf Max Demand";
        private const string PERIOD = "Period";
        private const string DLS_ACTIVE = "DLS Active";
        private const string BILLING_RESET_COUNT = "Billing Reset Count";
        private const string BILLING_RESET_DATETIME = "Billing Reset Date/Time";
        private const string RATE = "Rate";
        #endregion

        private readonly IInputReader<string, string> inputReader;

        public TouRecordList(IInputReader<string, string> inputReader) {
            if (inputReader == null)
                throw new ArgumentNullException(nameof(inputReader));

            this.inputReader = inputReader;
        }

        public override IEnumerator<TouRecord> GetEnumerator()
        {
            return new Enumerator(this);
        }

        private class Enumerator : IEnumerator<TouRecord> {
            private readonly TouRecordList parent;
            private readonly IInputReader<string, string> inputReader;

            private TouRecord current;

            public Enumerator(TouRecordList parent) {
                this.parent = parent;
                this.inputReader = this.parent.inputReader;
                this.inputReader.Reset();
            }

            public bool MoveNext() {
                if (!this.inputReader.HasNext()) return false;

                var properties = this.inputReader.ReadNext();

                var record = new TouRecord();

                // Set inherited properties
                this.parent.SetBasicProperties(record, properties);

                // Set other properties
                record.Energy = this.parent.ReadDoubleValue(ENERGY, properties);
                record.MaximumDemand = this.parent.ReadDoubleValue(MAXIMUM_DEMAND,
                    properties);
                record.MaxDemandDateTime = this.parent.ReadDateTimeValue(
                    MAX_DEMAND_DATETIME, properties);
                record.Period = this.parent.ReadStringValue(PERIOD, properties);
                record.DlsActive = this.parent.ReadBoolValue(DLS_ACTIVE, properties);
                record.BillingResetCount = this.parent.ReadIntValue(BILLING_RESET_COUNT,
                    properties);
                record.BillingResetDateTime = this.parent.ReadDateTimeValue(
                    BILLING_RESET_DATETIME, properties);
                record.Rate = this.parent.ReadStringValue(RATE, properties);

                // Set current item
                this.current = record;

                return true;
            }

            public void Reset() {
                this.inputReader.Reset();
                this.current = null;
            }

            public TouRecord Current {
                get {
                    if (this.current == null) {
                        throw new InvalidOperationException();
                    }
                    return this.current;
                }
            }

            object IEnumerator.Current => Current;

            public void Dispose() { }
        }
    }
}
