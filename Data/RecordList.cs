﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ErmProcessor.Data {
    public abstract class RecordList<T> : IEnumerable<T>
        where T : Record {
        #region Constant Property Keys
        protected const string METER_POINT = "MeterPoint Code";
        protected const string SERIAL_NUMBER = "Serial Number";
        protected const string PLANT_CODE = "Plant Code";
        protected const string DATE_TIME = "Date/Time";
        protected const string DATA_TYPE = "Data Type";
        protected const string UNITS = "Units";
        protected const string STATUS = "Status";
        #endregion

        public abstract IEnumerator<T> GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() {
            return this.GetEnumerator();
        }

        protected void SetBasicProperties(Record r,
            IDictionary<string, string> properties) {
            r.MeterPointCode = this.ReadIntValue(METER_POINT, properties);
            r.SerialNumber = this.ReadIntValue(SERIAL_NUMBER, properties);
            r.PlantCode = this.ReadStringValue(PLANT_CODE, properties);
            r.DateTime = this.ReadDateTimeValue(DATE_TIME, properties);
            r.DataType = this.ReadStringValue(DATA_TYPE, properties);
            r.Units = this.ReadStringValue(UNITS, properties);
            r.Status = this.ReadStringValue(STATUS, properties);
        }

        protected int? ReadIntValue(string key,
            IDictionary<string, string> properties) {

            if (!properties.ContainsKey(key)) return null;

            string stringValue = properties[key];
            if (!int.TryParse(stringValue, out int val)) {
                throw new ParsingException($"Invalid value {stringValue} for " +
                                           $"property {key}. Expected int.");
            }

            return val;
        }

        protected double? ReadDoubleValue(string key,
           IDictionary<string, string> properties) {

            if (!properties.ContainsKey(key)) return null;

            string stringValue = properties[key];
            if (!double.TryParse(stringValue, out double val)) {
                throw new ParsingException($"Invalid value {stringValue} for " +
                                           $"property {key}. Expected int.");
            }

            return val;
        }

        protected DateTime? ReadDateTimeValue(string key,
           IDictionary<string, string> properties) {

            if (!properties.ContainsKey(key)) return null;

            string stringValue = properties[key];
            if (!DateTime.TryParse(stringValue, out DateTime val)) {
                throw new ParsingException($"Invalid value {stringValue} for " +
                                           $"property {key}. Expected int.");
            }

            return val;
        }

        protected bool? ReadBoolValue(string key,
            IDictionary<string, string> properties) {

            if (!properties.ContainsKey(key)) return null;

            string stringValue = properties[key];
            if (!bool.TryParse(stringValue, out bool val)) {
                throw new ParsingException($"Invalid value {stringValue} for " +
                                           $"property {key}. Expected int.");
            }

            return val;
        }

        // Only for consistency
        protected string ReadStringValue(string key,
           IDictionary<string, string> properties) {

            if (!properties.ContainsKey(key)) return null;

            return properties[key];
        }
    }
}
