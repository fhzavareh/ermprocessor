﻿using System;
using System.Collections.Generic;

namespace ErmProcessor.Classifiers {
    public class SimpleOutlierClassifier<T> {
        private const double MARGIN = .2;

        private readonly IEnumerable<T> allRecords;
        private readonly Func<T, double> valueFunc;
        private bool isSetUp = false;
        // Grammar clarification: Setup: noun, Set Up: verb
        private double median;
        public double Median {
            get {
                this.CheckSetUp();
                return this.median;
            }
            set { this.median = value; }
        }

        public SimpleOutlierClassifier(IEnumerable<T> allRecords,
            Func<T, double> valueFunc) {

            this.allRecords = allRecords;
            this.valueFunc = valueFunc;
        }

        // NOTE: The purpose of this method is to avoid performing complex
        // and expensive operations in the constructor, which is considered 
        // bad practice
        /// <summary>
        /// Prepares the classifier. This method must be called before using
        /// the classifier.
        /// </summary>
        public virtual void SetUp() {
            if (!this.isSetUp) {
                this.Median = this.CalculateMedian(this.allRecords,
                    this.valueFunc);
                this.isSetUp = true;
            }
        }

        public virtual bool IsOutlier(T record) {
            this.CheckSetUp();

            double val = this.valueFunc(record);
            double max = this.median * (1 + MARGIN);
            double min = this.median * (1 - MARGIN);

            if (val >= max || val <= min) return true;
            // else
            return false;
        }

        /// <summary>
        /// Calculates the median by doing one pass over all allRecords. 
        /// This takes O(#allRecords) time.
        /// </summary>
        /// <returns>Median of all values.</returns>
        protected virtual double CalculateMedian(IEnumerable<T> records,
            Func<T, double> valueFunc) {
            double sum = 0;
            int count = 0;

            foreach (var record in records) {
                sum += valueFunc(record);
                count++;
            }

            return sum / count;
        }

        protected void CheckSetUp() {
            if (!this.isSetUp) {
                throw new InvalidOperationException("SetUp method must be " +
                    "called once before using the classifier.");
            }
        }

    }
}
