- Usage -
Run ConsoleInterface. Enter absolute path to the directory containing the CSV 
files.

- Remarks -
	* The program has constant space complexity (O(1)). Consequently, it can
	handle very large input files. For small files, this is slower than loading
	everything to memory. Right now, we are making two passes through each 
	file. Loading everything into memory would require only one pass per file.
	* New processors can easily be added by implementing IProcessor interface
	and plugging in the new processor at the top of the console main class.
	* The design follows SOLID principles. Notably, explicit dependencies 
	mean the design is dependency injection ready (dependency inversion 
	principle) and unit testable (e.g., one can use a mocking framework where 
	appropriate).
	* Developed with VS 2017
	
NOTE: VS 2015 version under VS2015 directory.